# Kores Stds

Defines some Kores standards

| Property          | Default value                      | Description |
| ----------------- |:----------------------------------:|:-----------:|
| `kores.debug.dir` | `$WORK_DIR/kores/$PROJECT/generated/debug/` | Generated classes directory (debug purpose)|